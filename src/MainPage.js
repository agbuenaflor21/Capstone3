import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Loader from './components/Loader';





// import our pages/React.lazy - loads our components - allows to run application faster
const Villa = React.lazy(() => import('./views/villas/Villa'));
const Home = React.lazy(() => import('./views/pages/Home'));
const Login = React.lazy(() => import('./views/pages/Login'));
const Register = React.lazy(() => import('./views/pages/Register'));
const Booking = React.lazy(() => import('./views/bookings/Booking'));
const MyBookings = React.lazy(() => import('./views/bookings/BookingsRow'));


const MainPage = () => {





  return (

    <BrowserRouter>
      <React.Suspense
        fallback={Loader}
      >
        <Switch>
          <Route
            path='/'
            exact
            render={props => <Home {...props} />}
          />
          <Route
            path='/home'
            render={props => <Home {...props} />}
          />
          <Route
            path='/login'
            render={props => <Login {...props} />}
          />
          <Route
            path='/register'
            render={props => <Register {...props} />}
          />
          <Route
            path='/register'
            render={props => <Register {...props} />}
          />
          <Route
            path='/villa'
            render={props => <Villa {...props} />}
          />
          <Route
            path='/booking'
            render={props => <Booking {...props} />}
          />
          <Route
            path='/mybookings'
            render={props => <MyBookings {...props} />}
          />
        </Switch>

      </React.Suspense>
    </BrowserRouter>

  );
}


export default MainPage;