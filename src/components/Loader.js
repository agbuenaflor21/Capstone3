import React from 'react';
import ReactLoading from 'react-loading';

const Loader = () => {
    return (
        <div
            className='d-flex justify-content-center align-items-center vh-100'
        >
            <ReactLoading
                color={'coral'}
                type={'cubes'}
                height={'30%'}
                width={'30%'}
            />

        </div>
    )
}

export default Loader;