import React, { useState, useEffect } from 'react';
import { FaMicrosoft } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './NavBar.css';
import logo from '../../assets/images/logo1.png';
import { Nav } from 'reactstrap';

const adminItems = [
    {
        name: 'Home',
        link: '/home'
    },
    {
        name: 'Register',
        link: '/register'
    },
    {
        name: 'Login',
        link: '/login'
    },

    {
        name: 'Villa',
        link: '/villa'
    },
    {
        name: 'All Bookings',
        link: '/mybookings'
    }

];

const userItems = [
    {
        name: 'Home',
        link: '/home'
    },
    {
        name: 'Register',
        link: '/register'
    },
    {
        name: 'Login',
        link: '/login'
    },
    {
        name: 'Book',
        link: '/booking'
    },
    {
        name: 'My Bookings',
        link: '/mybookings'
    }
]

const NavBar = (props) => {

    const [navItems, setNavItems] = useState([]);

    useEffect(() => {
        console.log(sessionStorage.isAdmin)

        // Remember that sessionStorage only stores values in string
        if (sessionStorage.isAdmin === 'true') {
            setNavItems(adminItems);
        } else {
            setNavItems(userItems);
        }
    }, [])

    return (
        <header className='toolbar'>
            <nav className='toolbar__navigation'>

                <div className='d-flex justify-content-between align-items-center' >
                    <div className='toolbar__logo2'><a href="/home">VISTA VENICE</a></div>
                    <div className='d-flex flex-row justify-content-center align-items-center navbarcontainer'>
                        {navItems.map((nav, index) => (
                            <Link
                                key={index}
                                to={nav.link}
                                style={{
                                    textDecoration: 'none',
                                    color: 'white'
                                }}
                            >
                                <div className='toolbar__logo ml-5 navbartext'>
                                    <a className='navbarA'>{nav.name}</a>
                                </div>

                            </Link>
                        ))}


                        {/* <div className='toolbar__logo ml-5 navbartext'><a href="/register" className='navbarA'>Register</a></div>
                        <div className='toolbar__logo ml-5  navbartext'><a href="/login" className='navbarA'>Login</a></div>
                        <div className='toolbar__logo ml-5  navbartext'><a href="/home" className='navbarA'>Home</a></div>
                        <div className='toolbar__logo ml-5  navbartext'><a href="/villa" className='navbarA'>Villa</a></div>
                        <div className='toolbar__logo ml-5  navbartext'><a href="/booking" className='navbarA'>Booking</a></div>
                        <div className='toolbar__logo ml-5  navbartext'><a href="/mybookings" className='navbarA'>My Bookings</a></div> */}



                    </div>
                </div>
            </nav>
        </header>
    )
};





// return (
//     <nav className='navbar bg-info'>
//         <div className='nav-center'>
//             <div className='nav-header d-flex'>
//                 <Link to='/'>
//                         <h1>Vista Venice Resort</h1>
//                     </Link>
//                 <h1>Vista Venice Resort</h1>
//                 <button
//                     type='button'
//                     className='nav-btn'
//                     onClick={handleToggle}
//                 >
//                     <FaMicrosoft className='nav-icon' />
//                 </button>
//             </div>
//             <ul className={isOpen ? "nav-links show-nav" : "nav-links"}>
//                     <li>
//                         <Link to="/">Home</Link>
//                     </li>
//                     <li>
//                         <Link to="/room">Rooms</Link>
//                     </li>
//                 </ul>
//         </div>

//     </nav>
// )
// }

export default NavBar;