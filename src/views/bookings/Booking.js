import React, { useState, useEffect } from 'react';
import { Card, CardBody, CardHeader, Table, Spinner, Row } from 'reactstrap';
import BookingsForm from './BookingsForm';
import BookingsRow from './BookingsRow';
import NavBar from '../../components/Toolbar/NavBar';
import { AddedToast } from '../../components/Toasts';
import Loader from '../../components/Loader';
import './bookings.css';
import VillaRow from '../villas/VillaRow';
import { useHistory } from 'react-router-dom';



const Booking = props => {
    const history = useHistory();
    const [villas, setVillas] = useState([])
    const [bookings, setBookings] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        fetch('https://pure-forest-29610.herokuapp.com/admin/villas')
            // console.log(sessionStorage.userId)
            .then(res => res.json())
            .then(res => {
                setVillas(res);
                setIsLoading(false);
            })
    }, [])


    const saveBooking = (checkIn, checkOut, villa, guests) => {
        const apiOptions = {
            method: 'POST',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({
                checkIn,
                checkOut,
                villa: villa._id,
                guests,
                payment: 'Pending',
                userId: sessionStorage.userId
            })
        }

        fetch('https://pure-forest-29610.herokuapp.com/admin/addbooking', apiOptions)
            .then(res => res.json())
            .then(res => {
                AddedToast('this booking');
                setIsLoading(false);
                res.villa = villa;
                const newBookings = [res, ...bookings];
                setBookings(newBookings);
            })
        history.push('/mybookings');
    }

    const cancelBooking = booking => {
        const apiOptions = {
            method: 'PATCH',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({
                id: booking._id,
                bookingId: booking.villa._id

            })
        }

        fetch('https://pure-forest-29610.herokuapp.com/admin/cancelbooking', apiOptions)
            .then(res => res.json())
            .then(res => {
                console.log(res);
                let newBookings = bookings.map(indivBooking => {
                    if (indivBooking._id === res._id) {
                        indivBooking.status = res.status;
                        indivBooking.payment = res.payment;
                    }

                    return indivBooking;
                });

                setBookings(newBookings);
            });
    }

    const updateBooking = id => {
        console.log(id);
        const newBookings = bookings.map(indivBooking => {
            if (indivBooking._id === id) {
                indivBooking.status = 'Paid';
                indivBooking.payment = 'Stripe';
            }
            return indivBooking;
        });
        setBookings(newBookings);
    }

    if (isLoading) return <Loader />

    return (
        <React.Fragment>

            <div style={{ marginTop: '0rem' }}>
                <NavBar />
            </div>
            <div>
                <BookingsForm
                    // showForm={showForm}
                    // toggleShowForm={toggleShowForm}
                    saveBooking={saveBooking}
                />
                <br />
                <div className='villacardcontainer'>
                    <Row className='col-12'>
                        {villas.map(villa => (
                            <VillaRow
                                key={villa._id}
                                villa={villa}
                                villas={villas}
                                setVillas={setVillas}
                            // deleteVilla={deleteVilla}
                            // editVilla={editVilla}
                            // setShowForm={setShowForm}
                            // setIsEditing={setIsEditing}
                            // setItemToEdit={setItemToEdit}
                            />
                        ))}
                    </Row>
                </div>
            </div>




            {/* <div>
                <Card className='containerBooking'>
                    <CardHeader className='text-center cardheader'>My Bookings</CardHeader>
                   
                    <CardBody>
                        <Table className='table-stripe'>
                            <thead>
                                <tr className='labelcontainer1'>
                                    <th style={{ color: 'white' }}>Check-In</th>
                                    <th style={{ color: 'white' }}>Check-Out</th>
                                    <th style={{ color: 'white' }}>Guests</th>
                                    <th style={{ color: 'white' }}>Room</th>
                                    <th style={{ color: 'white' }}>Amount</th>
                                    <th style={{ color: 'white' }}>Status</th>
                                    <th style={{ color: 'white' }}>Payment</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {bookings.map(booking => (
                                    <BookingsRow
                                        key={booking._id}
                                        booking={booking}
                                        cancelBooking={cancelBooking}
                                        updateBooking={updateBooking}
                                        bookings={bookings}
                                    />
                                ))}
                            </tbody>
                        </Table>
                    </CardBody>

                </Card>
            </div>
 */}




        </React.Fragment>
    )
}

export default Booking;