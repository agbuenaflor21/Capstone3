import React, { useState, useEffect } from 'react';
import DatePicker from 'react-datepicker';
import { Modal, ModalHeader, ModalBody, Label, Input, FormGroup, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Button } from 'reactstrap';
import './bookings.css';
import { compareAsc, format } from 'date-fns'

const BookingsForm = ({ saveBooking }) => {



    const [villas, setVillas] = useState([]);
    const [isOpenVillas, setIsOpenVillas] = useState(false);

    // fields we need
    const [villa, setVilla] = useState(null);
    const [guests, setGuests] = useState(0);
    const [checkIn, setCheckIn] = useState(new Date());
    const [checkOut, setCheckOut] = useState(new Date());



    console.log(new Date)

    useEffect(() => {
        fetch('https://pure-forest-29610.herokuapp.com/admin/villas')
            .then(res => res.json())
            .then(res => {
                setVillas(res);
            });
    }, []);
    console.log(setVillas);

    const handleBooking = () => {
        window.location.replace('/mybookings');
    }

    // const handleCheckIn = (date) => {
    //     var date = new Date();
    //     var options = { year: 'numeric', month: 'short', day: '2-digit' };
    //     var _resultDate = new Intl.DateTimeFormat('en-GB', options).format(date);
    //     setCheckIn(_resultDate);
    // }




    return (
        <div className=' d-flex flex-column justify-content-center align-items-center containerBookingForm' >
            <div className='headercontainer'>
                <h1 className='headertext'>LIVE | PLAY | TRAVEL</h1>
            </div>
            <div className='d-flex flex-row justify-content-center align-items-center mt-5'>
                <FormGroup
                    style={{ marginRight: '1rem' }}
                    className='labelcontainer'
                >
                    <Label className='bookinglabel'>Check-In: </Label>
                    <br />
                    <DatePicker
                        selected={checkIn}
                        onChange={date => setCheckIn(date)}
                        selectsStart
                        startDate={checkIn}
                        endDate={checkOut}
                        isClearable
                        placeholderText='Choose Date'
                        showDisabledMonthNavigation
                        minDate={new Date()}
                        dateFormat="MM/dd/yyyy"


                    // maxDate={addMonths(new Date(), 12)}
                    />
                </FormGroup>

                <FormGroup
                    style={{ marginRight: '1rem' }}
                    className='labelcontainer'
                >
                    <Label className='bookinglabel'>Check-Out: </Label>
                    <br />
                    <DatePicker
                        selected={checkOut}
                        onChange={date => setCheckOut(date)}
                        selectsEnd
                        startDate={checkIn}
                        endDate={checkOut}
                        isClearable
                        placeholderText='Choose Date'
                        showDisabledMonthNavigation
                        minDate={new Date()}
                        dateFormat="MM/dd/yyyy"

                    />
                </FormGroup>
                <FormGroup
                    style={{ marginRight: '1rem' }}
                    className='labelcontainer'
                >
                    <label className='bookinglabel'>Guests:</label>
                    <Input
                        style={{ width: '10rem', height: '2rem' }}
                        placeholder='Number of guests'
                        type='number'
                        onChange={(e) => setGuests(e.target.value)}
                    />
                </FormGroup>
                <FormGroup
                    style={{ marginRight: '1rem' }}
                    className='labelcontainer'
                >
                    <label className='bookinglabel'>Room:</label>
                    <Dropdown
                        isOpen={isOpenVillas}
                        toggle={() => setIsOpenVillas(!isOpenVillas)}

                    >
                        <DropdownToggle
                            className='bg-white'
                            color='black'
                            caret>
                            {!villa
                                ?
                                'Choose Villa'
                                :
                                villa.name
                            }
                        </DropdownToggle>
                        <DropdownMenu >
                            {villas.map(indivVilla => (
                                <DropdownItem
                                    key={indivVilla._id}
                                    onClick={() => setVilla(indivVilla)}
                                >{indivVilla.name}</DropdownItem>

                            ))}

                        </DropdownMenu>

                    </Dropdown>
                </FormGroup>
            </div>
            <div>
                <Button
                    style={{ backgroundColor: 'coral' }}
                    className='bookingbutton'
                    size='lg'

                    onClick={() => { saveBooking(checkIn, checkOut, villa, guests) }}
                >

                    <span> Book Now</span></Button>
            </div>

        </div>

    )
}

export default BookingsForm;