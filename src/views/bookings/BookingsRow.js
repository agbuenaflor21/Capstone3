import React, { useState, useEffect } from 'react';
import { Button, Card, CardHeader, CardBody, Table } from 'reactstrap';
import PaymentForm from './PaymentForm';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import DownloadTicket from './DownloadTicket';
import NavBar from '../../components/Toolbar/NavBar';

const stripePromise = loadStripe('pk_test_uIeipTDzXlSjf5httG1aaWS500NWxcts3c')


const BookingsRow = (props) => {

    const [bookings, setBookings] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        fetch('https://pure-forest-29610.herokuapp.com/admin/bookings/' + sessionStorage.userId)
            // console.log(sessionStorage.userId)
            .then(res => res.json())
            .then(res => {
                setBookings(res);
                setIsLoading(false);
            })
    }, [])

    const [showPaymentForm, setShowPaymentForm] = useState(false);

    const updateBooking = id => {
        console.log(id);
        const newBookings = bookings.map(indivBooking => {
            if (indivBooking._id === id) {
                indivBooking.status = 'Paid';
                indivBooking.payment = 'Stripe';
            }
            return indivBooking;
        });
        setBookings(newBookings);
    }

    const cancelBooking = booking => {
        const apiOptions = {
            method: 'PATCH',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({
                id: booking._id,
                bookingId: booking.villa._id

            })
        }

        fetch('https://pure-forest-29610.herokuapp.com/admin/cancelbooking', apiOptions)
            .then(res => res.json())
            .then(res => {
                console.log(res);
                let newBookings = bookings.map(indivBooking => {
                    if (indivBooking._id === res._id) {
                        indivBooking.status = res.status;
                        indivBooking.payment = res.payment;
                    }

                    return indivBooking;
                });

                setBookings(newBookings);
            });
    }

    return (
        <>

            <NavBar />

            <div className='bookingrowcontainer'>
                <div>
                    <CardHeader className='text-center cardheader d-flex justify-content-center align-items-center'><a className='headingtext'>My Bookings</a></CardHeader>
                </div>
                <Card className='containerBooking' style={{ marginTop: '1rem' }}>

                    <CardBody>
                        <Table className='table-stripe'>
                            <thead>
                                <tr className='labelcontainer1'>
                                    <th style={{ color: 'white' }}>Check-In</th>
                                    <th style={{ color: 'white' }}>Check-Out</th>
                                    <th style={{ color: 'white' }}>Guests</th>
                                    <th style={{ color: 'white' }}>Room</th>
                                    <th style={{ color: 'white' }}>Amount</th>
                                    <th style={{ color: 'white' }}>Status</th>
                                    <th style={{ color: 'white' }}>Payment</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {bookings.map(booking => (
                                    <tr>
                                        <td>{booking.checkIn}</td>
                                        <td>{booking.checkOut}</td>
                                        <td>{booking.guests}</td>
                                        <td>{booking.villa.name} - {booking.villa.room}</td>
                                        <td>{booking.villa.price}</td>
                                        <td>{booking.status}</td>
                                        <td>{booking.payment}</td>
                                        <td>
                                            <Button
                                                color='danger'
                                                disabled={booking.status === 'Pending' ? false : true}
                                                onClick={() => cancelBooking(booking)}
                                            >Cancel</Button>{' '}
                                            <Button
                                                color='success'
                                                disabled={booking.payment === 'Stripe' || booking.status === 'Cancelled'}
                                                onClick={() => setShowPaymentForm(true)}
                                            >Pay via Stripe</Button>{' '}
                                            {booking.status === 'Paid' && <DownloadTicket booking={booking} />}
                                            <Elements stripe={stripePromise}>
                                                <PaymentForm
                                                    setShowPaymentForm={setShowPaymentForm}
                                                    showPaymentForm={showPaymentForm}
                                                    amount={booking.villa.price}
                                                    bookingId={booking._id}
                                                    updateBooking={updateBooking}
                                                />

                                            </Elements>


                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </CardBody>

                </Card>






            </div>
        </>
    )
}

export default BookingsRow;