import React, { useState, useEffect } from 'react';
import { Document, Page, View, Text, StyleSheet, PDFDownloadLink } from '@react-pdf/renderer';
import { Button } from 'reactstrap';
import './bookings.css';

const styles = StyleSheet.create({
    header: {
        height: '200px',
        width: '100%',
        textAlign: 'center',
        backgroundColor: 'darkseagreen'
        // display: 'flex',
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    brandName: {
        fontSize: 40,
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white',
        marginTop: '60px'
    },
    detailsContainer: {
        display: 'flex',
        justifyContent: 'center'
    },
    details: {
        margin: '5% 20%'
    },
    detailText: {
        color: 'darkgray',
        textAlign: 'center'
    }
})

const Ticket = ({ booking }) => {
    return (
        <Document>
            <Page size='A4'>
                <View
                    style={styles.header}
                >
                    <Text style={styles.brandName}>Vista Venice Resort</Text>
                </View>
                <View
                    style={styles.detailsContainer}
                >
                    <View style={styles.details}>
                        <Text style={styles.brandName}>Booking Voucher</Text>

                        <Text style={styles.detailText}>Check-In: {booking.checkIn}</Text>

                        <Text style={styles.detailText}>Check-Out: {booking.checkOut}</Text>

                        <Text style={styles.detailText}>Guests: {booking.guests}</Text>

                        <Text style={styles.detailText}>Villa: {booking.villa.name} - {booking.villa.room}</Text>

                        <Text style={styles.detailText}>Price: {booking.price}</Text>

                        <Text style={styles.detailText}>Status: {booking.status}</Text>

                        <Text style={styles.detailText}>Paid by: {sessionStorage.userName}</Text>
                    </View>
                    <View style={styles.details}>
                        <Text style={styles.detailText}>This ticket is non-refundable</Text>
                    </View>
                </View>
            </Page>
        </Document>
    )
};

const DownloadTicket = ({ booking }) => {

    const [open, setOpen] = useState(false);

    useEffect(() => {
        setOpen(false);
        setOpen(true);
        return () => setOpen(false);
    });

    return (
        <Button>
            {
                open && <PDFDownloadLink
                    document={<Ticket booking={booking} />}
                    fileName={"Ticket#" + Date.now() + ".pdf"}
                >
                    {({ loading }) => loading ? "Loading..." : 'Download Ticket'}
                </PDFDownloadLink>
            }

        </Button>
    )
}

export default DownloadTicket;

