import React from 'react';
import { Modal, ModalHeader, ModalBody, FormGroup, Button } from 'reactstrap';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { SuccessToast, ErrorToast } from '../../components/Toasts';

const PaymentForm = ({ showPaymentForm, setShowPaymentForm, amount, bookingId, updateBooking }) => {

    const stripe = useStripe();
    const elements = useElements();

    const saveCharge = async () => {
        const cardElement = elements.getElement(CardElement);

        const { token } = await stripe.createToken(cardElement)

        const apiOptions = {
            method: 'POST',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({
                token: token.id,
                email: sessionStorage.email,
                amount: amount * 100,
                id: bookingId
            })
        }

        const paymentResponse = await fetch('https://pure-forest-29610.herokuapp.com/charge', apiOptions);

        if (paymentResponse.ok) {
            SuccessToast('Successfully paid');
            updateBooking(bookingId);
        } else {
            ErrorToast('An error occured. Please try again.');
        }
        setShowPaymentForm(false);
    }




    return (
        <Modal
            isOpen={showPaymentForm}
            toggle={() => setShowPaymentForm(false)}
        >
            <ModalHeader
                toggle={() => setShowPaymentForm(false)}
            >Pay</ModalHeader>
            <ModalBody>
                <FormGroup>
                    <CardElement />
                    <Button
                        color='info'
                        className='mt-3'
                        disabled={!stripe}
                        onClick={saveCharge}
                    >Pay

                </Button>
                </FormGroup>
            </ModalBody>
        </Modal>

    )
}

export default PaymentForm;