import React from 'react';
import './pages.css';

const Footer = () => {
    return (
        <>
            {/* <div class="container-fluid padding">
                <div class="row text-center padding">
                    <div class="col-12">
                        <h2>Connect</h2>
                    </div>
                    <div class="col-12 social padding">
                        <a href="#"><i class="fab fa-facebook"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-google-plus-g"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div> */}
            <footer className='footercontainer d-flex justify-content-center align-items-center'>
                <div class="container-fluid padding footerdiv">
                    <div class="row text-center">
                        <div class="col-md-4">
                            <h4>Vista Venice Resort</h4>
                            <hr class="light" />
                            <p>555-555-5555</p>
                            <p>gmail@gmail.com</p>
                            <p>Morong, Bataan</p>
                            <p>Philippines</p>
                        </div>
                        <div class="col-md-4">
                            <hr class="light" />
                            <h5>Our hours</h5>
                            <hr class="light" />
                            <p>Monday: 9am - 5pm</p>
                            <p>Saturday: 9am - 5pm</p>
                            <p>Sunday: closed</p>
                        </div>
                        <div class="col-md-4">
                            <hr class="light" />
                            <h5>Service Area</h5>
                            <hr class="light" />
                            <p>City, State 0000</p>
                            <p>City, State 0000</p>
                            <p>City, State 0000</p>
                            <p>City, State 0000</p>
                        </div>
                        <div class="col-12">
                            <hr class="light" />
                            <h5>&copy; Albert Gerard Buenaflor</h5>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}

export default Footer;