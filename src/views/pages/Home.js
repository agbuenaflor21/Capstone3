import React, { useState, useEffect } from 'react';
import NavBar from '../../components/Toolbar/NavBar';
import './pages.css';
import Loader from '../../components/Loader';
import { Button, Spinner } from 'reactstrap';
import { FaWindows } from 'react-icons/fa';
import HomeVideo from './HomeVideo';
import HomeServices from './HomeServices';
import Footer from './Footer';
import { useHistory } from 'react-router-dom';

const Home = props => {
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        if (!sessionStorage.token) {
            history.push('./login'); // transfer back to home page if there's no token received

        }
        setIsLoading(false);
    }, [])

    const handleHome = () => {
        history.push('/villa');
    }

    if (isLoading) return <Loader />

    return (
        <>
            <div className='container1'>
                <div>
                    <NavBar />
                </div>
                <div>
                    <div
                        className='d-flex flex-column justify-content-center align-items-center text-center'
                    // style={{ marginLeft: '300px', marginTop: '300px' }}
                    >

                        <h1 className='header'>SWEET ESCAPE</h1>
                        {/* <div>
                            <Spinner type="grow" color="primary" />
                            <Spinner type="grow" color="secondary" />
                            <Spinner type="grow" color="success" />
                            <Spinner type="grow" color="danger" />
                            <Spinner type="grow" color="warning" />
                            <Spinner type="grow" color="info" />
                            <Spinner type="grow" color="light" />
                            <Spinner type="grow" color="dark" />
                        </div> */}
                        <div className='hometextcontainer'>
                            <h1 style={{ color: 'white' }}>The finest resort in town. Welcome {sessionStorage.userName}!</h1>
                        </div>
                        <br />
                        <br />

                        <div>
                            <Button
                                className='homebutton'
                                size='lg'
                                block
                                onClick={handleHome}
                            ><span>VIEW ROOMS</span>

                            </Button>
                        </div>
                    </div>

                </div>


            </div>
            <div className='container2'>
                <HomeVideo />
            </div>
            <div className='container3'>
                <HomeServices />
            </div>
            <div className='container4'>
                <Footer />
            </div>
        </>
    )
}

export default Home;