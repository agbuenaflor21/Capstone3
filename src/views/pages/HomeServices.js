import React from 'react';
import { CardLink, Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText, Button } from 'reactstrap';
import './pages.css';
import bg10 from '../../assets/images/bg10.jpg';
import bg5 from '../../assets/images/bg5.jpg';
import bg4 from '../../assets/images/bg4.jpg';
import { IoIosArrowDroprightCircle } from "react-icons/io";

const HomeServices = () => {
    return (
        <>
            <div className='d-flex justify-content-center align-items-center flex-column'>
                <div className='d-flex flex-column justify-content-center align-items-center'>
                    <div>
                        <h1 className='servicesheader'>PACKAGES OFFERED</h1>
                        <hr color='coral' />
                    </div>
                    <div className='col-8 d-flex justify-content-center align-items-center'>
                        <h6 style={{ color: 'gray' }} className='text-center'>    Enjoy sweeping water views when you reserve a Hudson River View Suite. Spread out in an Executive or Luxury Suite with over 700 square feet of lavish living areas with integrated technology. Live the New York lifestyle in the Conrad Suite, featuring an office and separate living and dining areas.</h6>
                    </div>
                </div>
                <div className='container3 d-flex justify-content-around align-items-center'>
                    <Card className='cardcontainer'>
                        <CardImg top width="100%" src={bg10} alt="Card image cap" className='cardimage' />
                        <CardBody>
                            <div className='cardmiddle'>
                                <div className='cardtext'>Group package starts @Php 10,000</div>
                            </div>
                            <CardTitle>TEAM BUILDING</CardTitle>
                            <CardSubtitle>Card subtitle</CardSubtitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <CardLink href='/villa'><IoIosArrowDroprightCircle className='nav-icon' />   Learn More</CardLink>

                        </CardBody>
                    </Card>
                    <Card className='cardcontainer'>
                        <CardImg top width="100%" src={bg5} alt="Card image cap" className='cardimage' />
                        <CardBody>
                            <div className='cardmiddle'>
                                <div className='cardtext'>Vacation package starts @Php5,000</div>
                            </div>
                            <CardTitle>FAMILY VACATION</CardTitle>
                            <CardSubtitle>Card subtitle</CardSubtitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <CardLink href='/villa'><IoIosArrowDroprightCircle className='nav-icon' />   Learn More</CardLink>

                        </CardBody>
                    </Card>
                    <Card className='cardcontainer'>
                        <CardImg top width="100%" src={bg4} alt="Card image cap" className='cardimage' />
                        <CardBody>
                            <div className='cardmiddle'>
                                <div className='cardtext'>Wedding package starts @Php40,000</div>
                            </div>
                            <CardTitle>WEDDING</CardTitle>
                            <CardSubtitle>Card subtitle</CardSubtitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                            <CardLink href='/villa'><IoIosArrowDroprightCircle className='nav-icon' />   Learn More</CardLink>

                        </CardBody>
                    </Card>
                </div>
            </div >
        </>
    )
}

export default HomeServices;