import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import './pages.css';
import bg10 from '../../assets/images/bg10.jpg';

const HomeVideos = () => {
    return (
        <>
            <section>
                <div className="container-fluid padding d-flex justify-content-center align-items-center">
                    <div className="row welcome text-center">
                        <div className="col-12 ">
                            <h1 className="display-4" style={{ color: 'coral' }}></h1>
                            <hr />
                        </div>
                        <hr />
                    </div>
                </div>
                <div className="container-fluid padding ">
                    <div className="row padding d-flex justify-content-center align-items-center">
                        <div className="col-lg-5 text-center">
                            <h2 style={{ color: 'white' }}>Welcome to Paradise!</h2>
                            <br />

                            <p style={{ color: 'white' }}>All our Deluxe rooms have big windows to help you take a broad view of the cityscape and nature. We offer bigger bed and every bathroom has bathtub and shower, which brings relaxation to you after a long day.

Fast WIFI connection, satelite TV and international standard electric socket are standard throughout Hotel. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            {/* <br />
                            <a href="#" className="btn btn-primary">Learn More</a> */}
                        </div>
                        <div
                            className="col-lg-5 video"

                        >
                            <br />
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/f9u3y1yIYv0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </section>

        </>
    )
}

export default HomeVideos;