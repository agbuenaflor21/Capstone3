import React, { useState, useEffect } from 'react';
import { FormGroup, Label, Input, Button, CardLink } from 'reactstrap';
import { FaDivide } from 'react-icons/fa';
import './pages.css';
import { Link, useHistory } from 'react-router-dom';

const Login = props => {



    useEffect(() => {
        sessionStorage.clear();
    }, [])

    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = async () => {
        const apiOptions = {
            method: 'POST',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({
                email,
                password
            })
        }

        const fetchedData = await fetch('https://pure-forest-29610.herokuapp.com/login', apiOptions);
        const data = await fetchedData.json();
        console.log(data);

        sessionStorage.token = data.token; // save temporarily the token from login
        sessionStorage.userId = data.user.id;
        sessionStorage.isAdmin = data.user.isAdmin;
        sessionStorage.userName = data.user.firstName + " " + data.user.lastName;
        sessionStorage.email = data.user.email;

        history.push('/home');
    }

    return (
        <div
            className='logincontainer d-flex justify-content-end align-items-top vh-100'

        >
            <div className='loginheader d-flex justify-content-center align-items-center mr-5 mt-5'>
                <div
                    style={{ width: '15vw' }}

                >
                    <h1 className='text-center loginheadertext'>Login</h1>
                    <form>
                        <FormGroup>
                            <Label className='labeltext'>Email:</Label>
                            <Input
                                type='email'
                                placeholder='Enter your email'
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label className='labeltext'>Password:</Label>
                            <Input
                                type='password'
                                placeholder='Enter your password'
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </FormGroup>
                        <div className='d-flex flex-column justify-content-center align-items-center'>
                            <Button
                                className='loginbutton'
                                disabled={email === '' || password === '' ? true : false}
                                // color='warning'
                                onClick={handleLogin}
                            ><span>Login</span></Button>
                            <Link to='/register' className='text-white'>
                                No Account? Create one here
                            </Link>
                        </div>

                    </form>

                </div>

            </div>
        </div>
    )
}

export default Login;