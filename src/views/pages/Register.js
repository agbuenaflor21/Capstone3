import React, { useState } from 'react';
import { FormGroup, Label, Input, Button, CardLink } from 'reactstrap';
import './pages.css';
import { useHistory, Link } from 'react-router-dom';


const Register = props => {
    const history = useHistory();
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const handleRegister = async () => {
        const apiOptions = {
            method: 'POST',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                password
            })
        }
        // console.log(apiOptions);
        await fetch('https://pure-forest-29610.herokuapp.com/register', apiOptions);

        history.push('./login'); // transfer to login page right after registration
    }

    return (
        <div
            className='registercontainer d-flex justify-content-end align-items-center vh-100'

        >
            <div className='loginheader2 d-flex justify-content-center align-items-center mr-5'>
                <div style={{ width: '15vw' }}>
                    <h1 className='text-center loginheadertext'>Register</h1>
                    <form>
                        <FormGroup>
                            <Label className='labeltext'>First Name:</Label>
                            <Input
                                type='text'
                                placeholder='Enter your First Name'
                                onChange={(e) => setFirstName(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label className='labeltext'>Last Name:</Label>
                            <Input
                                type='text'
                                placeholder='Enter your password'
                                onChange={(e) => setLastName(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label className='labeltext'>Email:</Label>
                            <Input
                                type='email'
                                placeholder='Enter your email'
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label className='labeltext'>Password:</Label>
                            <Input
                                type='password'
                                placeholder='Enter your password'
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label className='labeltext'>Confirm Password:</Label>
                            <Input
                                type='password'
                                placeholder='Confirm Password'
                                onChange={(e) => setConfirmPassword(e.target.value)}
                            /><span className='text-danger'>{confirmPassword !== '' && confirmPassword !== password ? 'Passwords did not match' : ''}</span>
                        </FormGroup>
                        <div className='d-flex flex-column justify-content-center align-items-center'>
                            <Button
                                // color='warning'
                                className='loginbutton'
                                type='button'
                                disabled={firstName === '' || lastName === '' || email === '' || password === '' || password !== confirmPassword ? true : false}
                                onClick={handleRegister}
                            ><span>Register</span></Button>
                            <Link to='/login' style={{ color: 'white' }}>Already have an account?Log in here</Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Register;