import React, { useState, useEffect } from 'react';
import { Row, Button } from 'reactstrap';
import VillaForm from './VillaForm';
import VillaRow from './VillaRow';
import { IoIosAddCircle } from 'react-icons/io'
import NavBar from '../../components/Toolbar/NavBar';
import { AddedToast, DeleteToast } from '../../components/Toasts';
import Loader from '../../components/Loader';
import './villas.css';

const Villa = (props) => {

    const [showForm, setShowForm] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [itemToEdit, setItemToEdit] = useState({});
    const [villas, setVillas] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {

        setIsLoading(true);

        // fetch all villas
        fetch('https://pure-forest-29610.herokuapp.com/admin/villas')
            .then(res => res.json())
            .then(res => {
                setVillas(res);
                setIsLoading(false);
            });

    }, []);

    const addVilla = (name, description, image, preview, room, price) => {
        setIsLoading(true);
        if (isEditing) {
            let id = itemToEdit._id;
            let editedname = name;
            let editeddescription = description;
            let editedimage = image;
            let editedpreview = preview;
            let editedroom = room;
            let editedprice = price;

            if (editedname === '') { editedname = itemToEdit.name; }
            if (editeddescription === '') { editeddescription = itemToEdit.description; }
            if (editedimage === '') { editedimage = itemToEdit.image; }
            if (editedpreview === '') { editedpreview = itemToEdit.preview; }
            if (editedroom === '') { editedroom = itemToEdit.room; }
            if (editedprice === '') { editedprice = itemToEdit.price; }


            const apiOptions = {
                method: 'PATCH',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    id: itemToEdit._id,
                    name: editedname,
                    description: editeddescription,
                    coverImg: editedimage,
                    preview: editedpreview,
                    room: editedroom,
                    price: editedprice
                })
            }

            fetch('https://pure-forest-29610.herokuapp.com/admin/updatevilla', apiOptions)
                .then(res => res.json())
                .then(res => {
                    let newVillas = villas.map(villa => {
                        if (villa._id === itemToEdit._id) {
                            return res;
                        }
                        return villa;
                    });
                    setVillas(newVillas);
                    setIsEditing(false);
                    setShowForm(false);
                });
        } else {
            let apiOptions = {
                method: 'POST',
                headers: { 'Content-type': 'application/json' },
                body: JSON.stringify({
                    name,
                    description,
                    coverImg: image,
                    preview,
                    room,
                    price
                }),
            };

            fetch('https://pure-forest-29610.herokuapp.com/admin/addvilla', apiOptions)
                .then((res) => res.json())
                .then((res) => {
                    setIsLoading(false);
                    setVillas([res, ...villas]);
                });
        }
        setIsEditing(false);
        setShowForm(false);
    };


    const editVilla = (villa) => {
        setShowForm(true);
        setIsEditing(true);
        setItemToEdit(villa);
    };


    const toggleForm = () => {
        setShowForm(false);
        setItemToEdit({});
    }

    const deleteVilla = (id) => {
        fetch('https://pure-forest-29610.herokuapp.com/admin/deletevilla', {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id }),
        })
            .then(res => res.json())
            .then(res => {
                DeleteToast('this villa');
                const newVillas = villas.filter(villa => {
                    return villa._id != id
                });
                setVillas(newVillas);
            })
    }

    // const handleEdit = (villa) => {
    //     console.log(villa);
    //     setShowForm(true);
    //     setIsEditing(true);
    //     setItemToEdit(villa);

    // }

    // if (isLoading) return <Loader />

    return (
        <>

            <NavBar />


            <div
                className='d-flex justify-content-center align-items-center villatopcontainer'
            // style={{ backgroundColor: 'lightblue' }}
            >
                <div style={{ marginTop: '4rem' }} >
                    <div className=' d-flex flex-column justify-content-center villaheader'>

                        <div className='villabackground'>
                            <h1 className='text-center'>View Villas</h1>
                            <hr className='col-3' color='white' />
                        </div>
                        <br />
                        <div className='villabackground'>
                            <h6 className='col-8 offset-2 text-center' style={{ color: 'white' }}>All our Deluxe rooms have big windows to help you take a broad view of the cityscape and nature. We offer bigger bed and every bathroom has bathtub and shower, which brings relaxation to you after a long day.</h6>
                        </div>
                        <br />
                        <br />
                        <div className='d-flex justify-content-center align-items-center'>
                            <Button
                                onClick={() => {
                                    setShowForm(!showForm)
                                }}
                                className='villabutton'
                            >

                                <span>Add Villa</span> </Button>

                        </div>
                    </div>
                    <br />
                    <div className='villacardcontainer'>
                        <Row className='col-12'>
                            {villas.map(villa => (
                                <VillaRow
                                    key={villa._id}
                                    villa={villa}
                                    villas={villas}
                                    setVillas={setVillas}
                                    deleteVilla={deleteVilla}
                                    // editVilla={editVilla}
                                    setShowForm={setShowForm}
                                    setIsEditing={setIsEditing}
                                    setItemToEdit={setItemToEdit}
                                />
                            ))}
                        </Row>
                    </div>
                </div>

            </div>




            <VillaForm
                addVilla={addVilla}
                villas={villas}
                setVillas={setVillas}
                toggleForm={toggleForm}
                showForm={showForm}
                itemToEdit={itemToEdit}
                isEditing={isEditing}
                setShowForm={setShowForm}
                editVilla={editVilla}
            />

        </>
    )
}

export default Villa;