import React, { useState } from 'react';
import { Card, FormGroup, CardHeader, Label, Input, CardBody, Button, Modal, ModalHeader, ModalBody } from 'reactstrap'
import { AddedToast, ErrorToast } from "../../components/Toasts";
import './villas.css';


const VillaForm = ({ villas, setVillas, addVilla, editVilla, showForm, setShowForm, toggleForm, itemToEdit, isEditing }) => {

    const [villa, setVilla] = useState('');
    const [dropDownOpen, setDropDownOpen] = useState(false);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [image, setImage] = useState(null);
    const [preview, setPreview] = useState("");
    const [room, setRoom] = useState('');
    const [price, setPrice] = useState('');

    const handleSaveVilla = async () => {
        let mainImage = await uploadMainImage(image);
        // edit villa
        if (isEditing) {
            addVilla(
                name,
                description,
                mainImage.imageUrl,
                preview,
                room,
                price);
            setName(itemToEdit.name);
            setDescription(itemToEdit.description);
            setImage(itemToEdit.image);
            setPreview(itemToEdit.preview);
            setRoom(itemToEdit.room);
            setPrice(itemToEdit.price);

            // let id = itemToEdit._id;
            let editedname = name;
            let editeddescription = description;
            let editedimage = image;
            let editedpreview = preview;
            let editedroom = room;
            let editedprice = price;

            if (editedname === '') { editedname = itemToEdit.name; }
            if (editeddescription === '') { editeddescription = itemToEdit.description; }
            if (editedimage === '') { editedimage = itemToEdit.image; }
            if (editedpreview === '') { editedpreview = itemToEdit.preview; }
            if (editedroom === '') { editedroom = itemToEdit.room; }
            if (editedprice === '') { editedprice = itemToEdit.price; }

            // const apiOptions = {
            //     method: 'PATCH',
            //     headers: { 'Content-Type': 'application.json' },
            //     body: JSON.stringify({
            //         id: itemToEdit._id,
            //         name: editedname,
            //         description: editeddescription,
            //         image: editedimage,
            //         preview: editedpreview,
            //         room: editedroom,
            //         price: editedprice
            //     })
            // }

            // fetch('http://localhost:4000/admin/updatevilla', apiOptions)
            //     .then(res => res.json())
            //     .then(res => {
            //         let newVillas = villas.map(villa => {
            //             if (villa._id === itemToEdit._id) {
            //                 return res;
            //             }
            //             return villa;
            //         });
            //         setVillas(newVillas);
            //         // setIsEditing(false);
            //     });
        } else {
            // add villa
            addVilla(
                name,
                description,
                mainImage.imageUrl,
                preview,
                room,
                price);
            setName('');
            setDescription('');
            setImage('');
            setPreview('');
            setRoom('');
            setPrice('');
        };
        AddedToast('this villa');
    }

    const selectMainImage = e => {

        console.log(e.target.files)

        let image = e.target.files[0]

        console.log(image);

        if (image.name.match(/\.(jpg|JPG|JPEG|jpeg|png|PNG|gif)$/)) {
            setImage(image);
            console.log(image);
        } else {
            ErrorToast('Invalid Image. Please try again.');
        }
    }

    const uploadMainImage = async (imageToSave) => {
        const data = new FormData();
        data.append('image', image, imageToSave.name);

        const imageData = await fetch('https://pure-forest-29610.herokuapp.com/upload', {
            method: 'POST',
            body: data
        })

        const imageUrl = await imageData.json();

        return imageUrl;
    }
    console.log(itemToEdit);

    return (

        <Modal
            isOpen={showForm}
            toggle={toggleForm}

        >
            <ModalHeader
                toggle={() => setShowForm(!showForm)}
                className='villaformcontainer d-flex justify-content-center align-items-center text-center'
                style={{ color: 'gray' }}
            >
                <h1 className='text-center'>{isEditing ? 'Edit Villa' : 'Add Villa'}</h1>
            </ModalHeader>
            <ModalBody className='villaformcontainer'>
                <Card className='villaformcardbody'>
                    <CardBody>
                        <FormGroup>
                            <Label style={{ color: 'white' }}> Name:</Label>
                            <Input
                                placeholder='Name of Villa'
                                // name='name'
                                // type='text'
                                defaultValue={isEditing ? itemToEdit.name : ''}
                                onChange={(e) => setName(e.target.value)}
                            />
                        </FormGroup>
                        <Label style={{ color: 'white' }}>Description:</Label>
                        <FormGroup>
                            <Input
                                placeholder='Description of Villa'
                                // name='description'
                                // type='text'
                                defaultValue={isEditing ? itemToEdit.description : ''}
                                onChange={(e) => setDescription(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label style={{ color: 'white' }}>Image</Label>
                            <Input
                                placeholder='Image'
                                // name='image'
                                type='file'
                                onChange={selectMainImage}
                            // defaultValue={isEditing ? itemToEdit.image : ''}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label style={{ color: 'white' }}>Preview:</Label>
                            <Input
                                placeholder='Preview'
                                // name='image'
                                type='file'
                                // defaultValue={isEditing ? itemToEdit.preview : ''}
                                onChange={(e) => setPreview(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label style={{ color: 'white' }}>Room:</Label>
                            <Input

                                placeholder='Enter Room Number'
                                name='room'
                                type='text'
                                defaultValue={isEditing ? itemToEdit.room : ''}
                                onChange={(e) => setRoom(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label style={{ color: 'white' }}>Price:</Label>
                            <Input
                                placeholder='Enter Price'
                                // name='price'
                                // type='number'
                                defaultValue={isEditing ? itemToEdit.price : ''}
                                onChange={(e) => setPrice(e.target.value)}
                            />
                        </FormGroup>
                    </CardBody>
                    <Button
                        style={{ backgroundColor: '#f4511e' }}
                        onClick={() => {
                            handleSaveVilla(name, description, image, preview, room, price);
                            setName('');
                            setDescription('');
                            setImage('');
                            setPreview('');
                            setRoom('');
                            setPrice('');
                        }}
                    >{isEditing ? "Update Villa" : "Add Villa"}</Button>
                </Card>
            </ModalBody>
        </Modal >

    );

};

export default VillaForm;