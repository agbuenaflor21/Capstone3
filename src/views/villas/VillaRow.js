import React from 'react';
import { IoIosBrush, IoMdCloseCircle } from 'react-icons/io';
import { FaBed, FaCreativeCommonsBy, FaBath, FaMale, FaFemale, FaWifi } from "react-icons/fa";
import { Col, CardBody, CardHeader, CardImg, CardTitle, CardSubtitle, CardText } from 'reactstrap';

const VillaRow = ({
    villa,
    deleteVilla,
    setShowForm,
    setIsEditing,
    setItemToEdit

}) => {



    return (
        <React.Fragment>

            <Col className='col-4 offset-0 villarowcontainer'>

                {/* <CardHeader></CardHeader> */}
                <CardImg
                    src={'https://pure-forest-29610.herokuapp.com/' + villa.coverImg}
                // style={{ height: '50vh', width: '30vw' }}

                />
                <CardBody className='border' style={{ backgroundColor: 'white' }}>
                    <CardTitle className='villaname'>Villa: {villa.name}</CardTitle>
                    <CardSubtitle className='villatext' >Description: {villa.description}</CardSubtitle>
                    <CardText className='villatext'>Room #: {villa.room}</CardText>
                    <CardText className='villatext'>Price: {villa.price}</CardText>
                    <div className='d-flex flex-row'>
                        <FaBed className='nav-icon' />
                        <FaBath className='nav-icon ml-1' />
                        <FaMale className='nav-icon m1-1' />
                        <FaFemale className='nav-icon m1-1' />
                        <FaWifi className='nav-icon ml-1' />

                    </div>
                    <br />
                    <button
                        className='btn btn-info'
                        onClick={() => {
                            setShowForm(true);
                            setIsEditing(true);
                            setItemToEdit(villa);

                        }}>

                        <IoIosBrush className='nav-icon' /></button>
                    {''}
                    <button
                        className='btn btn-danger ml-2'
                        onClick={() => deleteVilla(villa._id)}
                    >

                        <IoMdCloseCircle className='nav-icon' />
                    </button>
                    <br />

                </CardBody>

            </Col>



        </React.Fragment>
    )
}

export default VillaRow;